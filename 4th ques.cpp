#include<iostream>

using namespace std;

class complex{
	public:
		int real;
		int imaginary;
		complex(int r,int i){real=r;imaginary=i;}
		complex operator + (complex const &obj){
			return complex(real+obj.real,imaginary+obj.imaginary);
		}
		void print(){
			cout<<real<<" + i"<<imaginary<<endl;
		}
};
int main(){
	
	complex c1(5,2),c2(4,3);
	complex c3= c1 + c2;
	c3.print();
	return 0;
}
