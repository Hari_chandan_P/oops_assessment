#include<iostream>

using namespace std;

class person{
	public:
		void talk(){cout<<"i can talk"<<endl;}
		void eat(){cout<<"i can eat"<<endl;}
		void walk(){cout<<"i can walk"<<endl;}
};
class mathsTeacher: public person{
	public:
	void teachMaths(){cout<<"i am teaching maths"<<endl;}
};
class footballer: public person{
	public:
	void playFootball(){cout<<"i am playing football"<<endl;}
};
class businessman: public person{
	public:
	void runBusiness(){cout<<"i am running business";}
};
int main(){
	person P;
	mathsTeacher M;
	footballer F;
	businessman B;
	cout<<"P says ";P.eat();
	cout<<"M says ";M.talk();cout<<" and ";M.teachMaths();
	cout<<"F says ";F.playFootball();
	return 0;
}
