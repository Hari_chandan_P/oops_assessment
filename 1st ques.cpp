#include<iostream>

using namespace std;

//default constructor
class type1  
 {  
   public:  
        type1()    
        {    
            cout<<"hello world"<<endl;    
        }    
};
//Parameterized constructor
 class type2
 {
 	public:
 		int id;
		string name;
 		type2(int i,string n){
 			id=i;
 			name=n;
		 }
		 void display(){
		 	cout<<"hey "<<name<<" your id: "<<id<<endl;
		 }
}; 
int main(void)   
{  
    type1 t1;   
    type2 t2=type2(9, "hari");
	t2.display();
    return 0;  
}  

 

