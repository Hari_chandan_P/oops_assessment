#include<iostream>

using namespace std;

class Animal{
	public:
	void sound(){
		cout<<"I am animal."<<endl;
	}
};
class Dog: public Animal {
	public:
	void speak(){
		cout<<"dog says ";
	}
};
class Lion: public Animal{
	public:
	void speak(){
		cout<<"lion says ";
	}
};
int main(){
	Animal A;cout<<"any animal can say ";A.sound();
	Dog D;Lion L;
	D.speak();D.sound();
	L.speak();L.sound();
	return 0;
}
