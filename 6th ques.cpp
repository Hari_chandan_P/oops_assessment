#include<iostream>

using namespace std;

//here data member is private and cannot be accessed by outside class.
//using public method to access data member. 
//hiding data and showing the required info gives the example of encapsulation.
class encap{
	private:
		int d;
	public:
		void set(int a){
			d=a;
		}
		void get(){
			cout<<d<<endl;
		}
};
int main(){
	encap example;
	example.set(9);
	example.get();
	return 0;
}
